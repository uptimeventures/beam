// @flow

// Copyright 2017 Uptime Ventures, Ltd. All rights reserved.
// Use of this source code is governed by a BSD-style license
// that can be found in LICENSE.md, at the root of this repository.

export default class Beam {
  _e = {}

  on(name: string, fn: Function) {
    (this._e[name] || (this._e[name] = [])).push(fn)
    return this
  }

  off(name: string, fn: Function) {
    if (name in this._e) {
      this._e = this._e[name].splice(
        this._e[name].indexOf(fn), 1
      )
    }
    return this
  }

  once(name: string, fn: Function) {
    const l = e => {
      fn(e)
      this.off(name, fn)
    }
    return this.on(name, l)
  }

  emit(name: string, ...args: Array<any>) {
    if (name in this._e) {
      this._e[name].forEach(e => {
        e.apply(this, args)
      })
    }
    return this
  }
}
