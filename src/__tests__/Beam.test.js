import Beam from '../Beam'

describe('Beam', () => {
  it('fires events on emit', () => {
    const fn = jest.fn()
    const e = new Beam()
    e.on('alert', fn)
    e.emit('alert')
    expect(fn.mock.calls.length).toEqual(1)
  })

  it('tracks multiple events on emit', () => {
    const fn = jest.fn()
    const e = new Beam()
    e.on('alert', fn)
    e.on('alert', fn)
    e.emit('alert')
    expect(fn.mock.calls.length).toEqual(2)
  })

  it('supports unsubscribing handlers', () => {
    const fn1 = jest.fn()
    const fn2 = jest.fn()
    const e = new Beam()
    e.on('alert', fn1)
    e.off('alert', fn1)
    e.on('alert', fn2)
    e.emit('alert')
    expect(fn1.mock.calls.length).toEqual(0)
    expect(fn2.mock.calls.length).toEqual(1)
  })

  it('supports one off handlers', () => {
    const fn = jest.fn()
    const e = new Beam()
    e.once('alert', fn)
    e.emit('alert')
    e.emit('alert')
    e.emit('alert')
    expect(fn.mock.calls.length).toEqual(1)
  })

  it('passes context', () => {
    let email, email2
    const e = new Beam()
    e.on('message', ({ email: e }) => {
      email = e
    })
    e.once('test', ({ email: ex }) => {
      email2 = ex
    })
    e.emit('message', { email: 'test@example.com' })
    e.emit('test', { email: 'test@example.com' })
    expect(email).toEqual('test@example.com')
    expect(email2).toEqual('test@example.com')
  })
})
