# Beam :satellite:

[![CI Status
Badge](https://travis-ci.org/uptimeventures/beam.svg?branch=master)](https://travis-ci.org/uptimeventures/beam)
[![NSP Status](https://nodesecurity.io/orgs/uptimeventures/projects/dc542e76-a1a4-4b2e-8c90-37a9ebfd5e92/badge)](https://nodesecurity.io/orgs/uptimeventures/projects/dc542e76-a1a4-4b2e-8c90-37a9ebfd5e92)
[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)
[![Made by Uptime Ventures
badge](https://img.shields.io/badge/made_by-Uptime_Ventures-fcb040.svg)](https://www.uptime.ventures)

A micro event emitter, smaller than 500k when gzip'd.

## Installation

`npm install @uptimeventures/beam`

## Usage

Beam provides essentially the same interface as it's larger Node.js cousin:

```javascript
import Beam from '@uptimeventures/beam'

const emitter = new Beam()

beam.once('message', () => {
  console.log('only tell me about this once')
})

beam.on('alert', (msg) => {
  alert(msg)
})

beam.on('message', ({ name, email }) => {
  console.log(`Hey, ${name}. You have a new message from ${email}.`)
})

beam.emit('alert', 'Something went wrong')
beam.emit('message', { name: 'Nicholas', email: 'n@example.com' })
```

Inheriting from Beam is a breeze, too:

```javascript
import Beam from '@uptimeventures/beam'

class MyEmitter extends Beam {
}
```

## License

&copy; 2017 Uptime Ventures, Ltd. All rights reserved. Released under the
[3-Clause BSD License](LICENSE.md).
