const isCI = process.env.CI !== ''

module.exports = {
  notify: isCI ? false : true,
  verbose: true,
  roots: ['src/'],
  testPathIgnorePatterns: [
    '/examples/',
    '/node_modules/',
  ],
}
